#!/bin/sh

python conll.py \
  data/splits/train.sent \
  data/output/train.pos.brill \
  data/splits/train.tags \
  data/output/train.tags.jordan > data/output/train.conll.jordan

python conll.py \
  data/splits/valid.sent \
  data/output/valid.pos.brill \
  data/splits/valid.tags \
  data/output/valid.tags.jordan > data/output/valid.conll.jordan

python conll.py \
  data/splits/test.sent \
  data/output/test.pos.brill \
  data/splits/test.tags \
  data/output/test.tags.jordan > data/output/test.conll.jordan

perl conlleval.pl < data/output/train.conll.jordan
perl conlleval.pl < data/output/valid.conll.jordan
perl conlleval.pl < data/output/test.conll.jordan

#!/bin/sh

source nltk-trainer/.venv/bin/activate

python nltk-trainer/train_tagger.py treebank --brill \
  --nlp_train_sent data/splits/train.sent \
  --nlp_train_tags data/splits/train.tags \
  --nlp_test_sent data/splits/valid.sent \
  --nlp_test_tags data/splits/valid.tags

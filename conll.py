import argparse
from itertools import izip

def main(sent, pos, correct, predict):
  with open(sent) as f_sent, open(pos) as f_pos, open(correct) as f_correct, open(predict) as f_predict:
    for aa, bb, cc, dd in izip(f_sent, f_pos, f_correct, f_predict):
      combined = zip(aa.strip().split(" "), bb.strip().split(" "), cc.strip().split(" "), dd.strip().split(" "))
      for a, b, c, d in combined:
        print a, b, c, d
      print

"""
Sample Usage:

python conll.py \
  sent \
  pos \
  chunks \
  predict
"""

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('sent', type=str)
  parser.add_argument('pos', type=str)
  parser.add_argument('correct', type=str)
  parser.add_argument('predict', type=str)
  args = parser.parse_args()

  main(args.sent, args.pos, args.correct, args.predict)

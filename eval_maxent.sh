#!/bin/sh

python conll.py \
  data/splits/train.sent \
  data/output/train.pos.brill \
  data/splits/train.tags \
  data/output/train.tags.maxent > data/output/train.conll.maxent

python conll.py \
  data/splits/valid.sent \
  data/output/valid.pos.brill \
  data/splits/valid.tags \
  data/output/valid.tags.maxent > data/output/valid.conll.maxent

python conll.py \
  data/splits/test.sent \
  data/output/test.pos.brill \
  data/splits/test.tags \
  data/output/test.tags.maxent > data/output/test.conll.maxent

perl conlleval.pl < data/output/train.conll.maxent
perl conlleval.pl < data/output/valid.conll.maxent
perl conlleval.pl < data/output/test.conll.maxent

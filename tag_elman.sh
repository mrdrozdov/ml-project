#!/bin/sh
THEANO_FLAGS="device=cpu" python is13/examples/elman-classify.py data/splits/train.sent > data/output/train.tags.elman
THEANO_FLAGS="device=cpu" python is13/examples/elman-classify.py data/splits/valid.sent > data/output/valid.tags.elman
THEANO_FLAGS="device=cpu" python is13/examples/elman-classify.py data/splits/test.sent > data/output/test.tags.elman

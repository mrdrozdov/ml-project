#!/bin/sh

python conll.py \
  data/splits/train.sent \
  data/output/train.pos.brill \
  data/splits/train.tags \
  data/output/train.tags.crf > data/output/train.conll.crf

python conll.py \
  data/splits/valid.sent \
  data/output/valid.pos.brill \
  data/splits/valid.tags \
  data/output/valid.tags.crf > data/output/valid.conll.crf

python conll.py \
  data/splits/test.sent \
  data/output/test.pos.brill \
  data/splits/test.tags \
  data/output/test.tags.crf > data/output/test.conll.crf

perl conlleval.pl < data/output/train.conll.crf
perl conlleval.pl < data/output/valid.conll.crf
perl conlleval.pl < data/output/test.conll.crf

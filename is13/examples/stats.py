from itertools import chain
import nltk
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.preprocessing import LabelBinarizer
import sklearn
import pycrfsuite

import numpy
import time
import sys
import subprocess
import os
import random

from is13.data import load
from is13.rnn.elman import model
from is13.metrics.accuracy import conlleval
from is13.utils.tools import shuffle, minibatch, contextwin

print(sklearn.__version__)

# load the dataset
fold = 3
train_set, valid_set, test_set, dic = load.atisfold(fold)
idx2label = dict((k,v) for v,k in dic['labels2idx'].iteritems())
idx2word  = dict((k,v) for v,k in dic['words2idx'].iteritems())

train_lex, train_ne, train_y = train_set
valid_lex, valid_ne, valid_y = valid_set
test_lex,  test_ne,  test_y  = test_set

train_sents = [[(idx2word[w], '_', idx2label[y]) for w, y in zip(words, labels)] for words, labels in zip(train_lex, train_y)]
valid_sents = [[(idx2word[w], '_', idx2label[y]) for w, y in zip(words, labels)] for words, labels in zip(valid_lex, valid_y)]
test_sents = [[(idx2word[w], '_', idx2label[y]) for w, y in zip(words, labels)] for words, labels in zip(test_lex, test_y)]

print("Using fold", fold)
print("# tags", len(dic['labels2idx'].keys()))
print("# B-tags", len([k for k in dic['labels2idx'].keys() if k.startswith("B")]))
print("# words", len(dic['words2idx'].keys()))
print("# train", len(train_sents))
print("# valid", len(valid_sents))
print("# test", len(test_sents))

import numpy
import time
import sys
import subprocess
import os
import random
import ipdb

from is13.data import load
from is13.rnn.elman import model
from is13.metrics.accuracy import conlleval
from is13.utils.tools import shuffle, minibatch, contextwin

if __name__ == '__main__':

    filename = sys.argv[1]
    with open(filename) as f_in:
        sentences = [s.strip().split(" ") for s in f_in]

    s = {'fold':3, # 5 folds 0,1,2,3,4
         'lr':0.0627142536696559,
         'verbose':1,
         'decay':False, # decay on the learning rate if improvement stops
         'win':7, # number of words in the context window
         'bs':9, # number of backprop through time steps
         'nhidden':100, # number of hidden units
         'seed':345,
         'emb_dimension':100, # dimension of word embedding
         'nepochs':50}

    # load the dataset
    train_set, valid_set, test_set, dic = load.atisfold(s['fold'])

    idx2label = dict((k,v) for v,k in dic['labels2idx'].iteritems())
    idx2word  = dict((k,v) for v,k in dic['words2idx'].iteritems())

    train_lex, train_ne, train_y = train_set
    valid_lex, valid_ne, valid_y = valid_set
    test_lex,  test_ne,  test_y  = test_set

    vocsize = len(dic['words2idx'])
    nclasses = len(dic['labels2idx'])
    nsentences = len(train_lex)

    # instanciate the model
    numpy.random.seed(s['seed'])
    random.seed(s['seed'])
    rnn = model(    nh = s['nhidden'],
                    nc = nclasses,
                    ne = vocsize,
                    de = s['emb_dimension'],
                    cs = s['win'] )
    folder = 'elman-forward'
    rnn.load(folder)

    for sent in sentences:
        # ipdb.set_trace()
        # print "Classifying", sent

        # Get the index of the word
        # TODO: Convert to <UNK> if unknown
        word_idxs = [dic['words2idx'][w] for w in sent]

        # classify the indices
        label_idxs = rnn.classify(numpy.asarray(contextwin(word_idxs, s['win'])).astype('int32'))

        # convert the predict indices into human readable tokens
        prediction = [idx2label[idx] for idx in label_idxs]

        # print out the embeddings for fun
        # embs = [rnn.emb[w] for w in word_idxs]

        # print " ".join(["_".join(zip(sent, prediction))])
        print " ".join(prediction)

    sys.exit()

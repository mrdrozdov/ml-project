import numpy
import time
import sys
import subprocess
import os
import random
import ipdb

from is13.data import load
from is13.rnn.elman import model
from is13.metrics.accuracy import conlleval
from is13.utils.tools import shuffle, minibatch, contextwin

unk_token = '<UNK>'

if __name__ == '__main__':

    filename = sys.argv[1]
    sentences = [s.split(" ") for s in open(filename).read().split("\n")]

    s = {'fold':3, # 5 folds 0,1,2,3,4
         'lr':0.0627142536696559,
         'verbose':1,
         'decay':False, # decay on the learning rate if improvement stops
         'win':7, # number of words in the context window
         'bs':9, # number of backprop through time steps
         'nhidden':100, # number of hidden units
         'seed':345,
         'emb_dimension':100, # dimension of word embedding
         'nepochs':50}

    folder = os.path.basename("elman-forward").split('.')[0]

    # load the dataset
    train_set, valid_set, test_set, dic = load.atisfold(s['fold'])

    idx2label = dict((k,v) for v,k in dic['labels2idx'].iteritems())
    idx2word  = dict((k,v) for v,k in dic['words2idx'].iteritems())

    train_lex, train_ne, train_y = train_set
    valid_lex, valid_ne, valid_y = valid_set
    test_lex,  test_ne,  test_y  = test_set

    vocsize = len(dic['words2idx'])
    nclasses = len(dic['labels2idx'])
    nsentences = len(train_lex)

    for sent in sentences:
        # ipdb.set_trace()
        # print "Classifying", sent

        new_sent = []
        for w in sent:
            if w in dic['words2idx']:
                new_sent.append(w)
            else:
                new_sent.append(unk_token)

        print " ".join(new_sent)

    sys.exit()

from itertools import chain
import nltk
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.preprocessing import LabelBinarizer
import sklearn
import pycrfsuite

import numpy
import time
import sys
import subprocess
import os
import random
from itertools import izip

import argparse

parser = argparse.ArgumentParser()
# parser.add_argument('tr_sent')
# parser.add_argument('tr_pos')
# parser.add_argument('tr_tags')
# parser.add_argument('te_sent')
# parser.add_argument('te_pos')
# parser.add_argument('te_tags')

parser.add_argument('--tr_sent', required=True, type=str)
parser.add_argument('--tr_pos', required=True, type=str)
parser.add_argument('--tr_tags', required=True, type=str)
parser.add_argument('--va_sent', required=True, type=str)
parser.add_argument('--va_pos', required=True, type=str)
parser.add_argument('--va_tags', required=True, type=str)
parser.add_argument('--te_sent', required=True, type=str)
parser.add_argument('--te_pos', required=True, type=str)
parser.add_argument('--te_tags', required=True, type=str)

parser.add_argument('--sa_model', required=True, type=str)
parser.add_argument('--sa_train', required=True, type=str)
parser.add_argument('--sa_valid', required=True, type=str)
parser.add_argument('--sa_test', required=True, type=str)

parser.add_argument('--skip_pos', action='store_true', default=False)
args = parser.parse_args()

if not args.skip_pos:
    print "Using POS"
else:
    print "Ignoring POS"

# Reading Training and Test Data

with open(args.tr_sent) as f_sent, open(args.tr_pos) as f_pos, open(args.tr_tags) as f_tags:
    train_sents = []
    for sent, pos, tags in izip(f_sent, f_pos, f_tags):
        ws, ps, ys = sent.strip().split(" "), pos.strip().split(" "), tags.strip().split(" ")
        train_sents.append([(w, p, y) for w, p, y in zip(ws, ps, ys)])

with open(args.va_sent) as f_sent, open(args.va_pos) as f_pos, open(args.va_tags) as f_tags:
    valid_sents = []
    for sent, pos, tags in izip(f_sent, f_pos, f_tags):
        ws, ps, ys = sent.strip().split(" "), pos.strip().split(" "), tags.strip().split(" ")
        valid_sents.append([(w, p, y) for w, p, y in zip(ws, ps, ys)])

with open(args.te_sent) as f_sent, open(args.te_pos) as f_pos, open(args.te_tags) as f_tags:
    test_sents = []
    for sent, pos, tags in izip(f_sent, f_pos, f_tags):
        ws, ps, ys = sent.strip().split(" "), pos.strip().split(" "), tags.strip().split(" ")
        test_sents.append([(w, p, y) for w, p, y in zip(ws, ps, ys)])

def word2features(sent, i):
    word = sent[i][0]
    postag = sent[i][1]
    features = [
        'bias',
        'word.lower=' + word.lower(),
        'word[-3:]=' + word[-3:],
        'word[-2:]=' + word[-2:],
        'word.isupper=%s' % word.isupper(),
        'word.istitle=%s' % word.istitle(),
        'word.isdigit=%s' % word.isdigit(),
    ]

    if not args.skip_pos:
        features.extend([
            'postag=' + postag,
            'postag[:2]=' + postag[:2],
            ])

    if i > 0:
        word1 = sent[i-1][0]
        postag1 = sent[i-1][1]
        features.extend([
            '-1:word.lower=' + word1.lower(),
            '-1:word.istitle=%s' % word1.istitle(),
            '-1:word.isupper=%s' % word1.isupper(),
        ])
        if not args.skip_pos:
            features.extend([
                '-1:postag=' + postag1,
                '-1:postag[:2]=' + postag1[:2],
                ])
    else:
        features.append('BOS')

    if i < len(sent)-1:
        word1 = sent[i+1][0]
        postag1 = sent[i+1][1]
        features.extend([
            '+1:word.lower=' + word1.lower(),
            '+1:word.istitle=%s' % word1.istitle(),
            '+1:word.isupper=%s' % word1.isupper(),
        ])
        if not args.skip_pos:
            features.extend([
                '+1:postag=' + postag1,
                '+1:postag[:2]=' + postag1[:2],
                ])
    else:
        features.append('EOS')

    return features

def sent2features(sent):
    return [word2features(sent, i) for i in range(len(sent))]

def sent2labels(sent):
    return [label for token, postag, label in sent]

def sent2tokens(sent):
    return [token for token, postag, label in sent]

X_train = [sent2features(s) for s in train_sents]
y_train = [sent2labels(s) for s in train_sents]

X_valid = [sent2features(s) for s in valid_sents]
y_valid = [sent2labels(s) for s in valid_sents]

X_test = [sent2features(s) for s in test_sents]
y_test = [sent2labels(s) for s in test_sents]

saveto = args.sa_model
tagger = pycrfsuite.Tagger()
tagger.open(saveto)

train_pred = [tagger.tag(xseq) for xseq in X_train]
valid_pred = [tagger.tag(xseq) for xseq in X_valid]
test_pred = [tagger.tag(xseq) for xseq in X_test]

with open(args.sa_train, "wb") as f_pred:
    for tags in train_pred:
        print >>f_pred, " ".join(tags)

with open(args.sa_valid, "wb") as f_pred:
    for tags in valid_pred:
        print >>f_pred, " ".join(tags)

with open(args.sa_test, "wb") as f_pred:
    for tags in test_pred:
        print >>f_pred, " ".join(tags)

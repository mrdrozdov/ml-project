from itertools import chain
import nltk
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.preprocessing import LabelBinarizer
import sklearn
import pycrfsuite

import numpy
import time
import sys
import subprocess
import os
import random
from itertools import izip

import argparse

parser = argparse.ArgumentParser()
# parser.add_argument('tr_sent')
# parser.add_argument('tr_pos')
# parser.add_argument('tr_tags')
# parser.add_argument('te_sent')
# parser.add_argument('te_pos')
# parser.add_argument('te_tags')

parser.add_argument('--tr_sent', required=True, type=str)
parser.add_argument('--tr_pos', required=True, type=str)
parser.add_argument('--tr_tags', required=True, type=str)
parser.add_argument('--va_sent', required=True, type=str)
parser.add_argument('--va_pos', required=True, type=str)
parser.add_argument('--va_tags', required=True, type=str)
parser.add_argument('--te_sent', required=True, type=str)
parser.add_argument('--te_pos', required=True, type=str)
parser.add_argument('--te_tags', required=True, type=str)

parser.add_argument('--sa_model', required=True, type=str)
parser.add_argument('--sa_train', required=True, type=str)
parser.add_argument('--sa_valid', required=True, type=str)
parser.add_argument('--sa_test', required=True, type=str)

parser.add_argument('--skip_pos', action='store_true', default=False)
args = parser.parse_args()

if not args.skip_pos:
    print "Using POS"
else:
    print "Ignoring POS"

# Reading Training and Test Data

with open(args.tr_sent) as f_sent, open(args.tr_pos) as f_pos, open(args.tr_tags) as f_tags:
    train_sents = []
    for sent, pos, tags in izip(f_sent, f_pos, f_tags):
        ws, ps, ys = sent.strip().split(" "), pos.strip().split(" "), tags.strip().split(" ")
        train_sents.append([(w, p, y) for w, p, y in zip(ws, ps, ys)])

with open(args.va_sent) as f_sent, open(args.va_pos) as f_pos, open(args.va_tags) as f_tags:
    valid_sents = []
    for sent, pos, tags in izip(f_sent, f_pos, f_tags):
        ws, ps, ys = sent.strip().split(" "), pos.strip().split(" "), tags.strip().split(" ")
        valid_sents.append([(w, p, y) for w, p, y in zip(ws, ps, ys)])

with open(args.te_sent) as f_sent, open(args.te_pos) as f_pos, open(args.te_tags) as f_tags:
    test_sents = []
    for sent, pos, tags in izip(f_sent, f_pos, f_tags):
        ws, ps, ys = sent.strip().split(" "), pos.strip().split(" "), tags.strip().split(" ")
        test_sents.append([(w, p, y) for w, p, y in zip(ws, ps, ys)])

def word2features(sent, i):
    word = sent[i][0]
    postag = sent[i][1]
    features = [
        'bias',
        'word.lower=' + word.lower(),
        'word[-3:]=' + word[-3:],
        'word[-2:]=' + word[-2:],
        'word.isupper=%s' % word.isupper(),
        'word.istitle=%s' % word.istitle(),
        'word.isdigit=%s' % word.isdigit(),
    ]

    if not args.skip_pos:
        features.extend([
            'postag=' + postag,
            'postag[:2]=' + postag[:2],
            ])

    if i > 0:
        word1 = sent[i-1][0]
        postag1 = sent[i-1][1]
        features.extend([
            '-1:word.lower=' + word1.lower(),
            '-1:word.istitle=%s' % word1.istitle(),
            '-1:word.isupper=%s' % word1.isupper(),
        ])
        if not args.skip_pos:
            features.extend([
                '-1:postag=' + postag1,
                '-1:postag[:2]=' + postag1[:2],
                ])
    else:
        features.append('BOS')

    if i < len(sent)-1:
        word1 = sent[i+1][0]
        postag1 = sent[i+1][1]
        features.extend([
            '+1:word.lower=' + word1.lower(),
            '+1:word.istitle=%s' % word1.istitle(),
            '+1:word.isupper=%s' % word1.isupper(),
        ])
        if not args.skip_pos:
            features.extend([
                '+1:postag=' + postag1,
                '+1:postag[:2]=' + postag1[:2],
                ])
    else:
        features.append('EOS')

    return features

def sent2features(sent):
    return [word2features(sent, i) for i in range(len(sent))]

def sent2labels(sent):
    return [label for token, postag, label in sent]

def sent2tokens(sent):
    return [token for token, postag, label in sent]

X_train = [sent2features(s) for s in train_sents]
y_train = [sent2labels(s) for s in train_sents]

X_valid = [sent2features(s) for s in valid_sents]
y_valid = [sent2labels(s) for s in valid_sents]

X_test = [sent2features(s) for s in test_sents]
y_test = [sent2labels(s) for s in test_sents]

trainer = pycrfsuite.Trainer(verbose=False)

for xseq, yseq in zip(X_train, y_train):
    trainer.append(xseq, yseq)

trainer.set_params({
    'c1': 1.0,   # coefficient for L1 penalty
    'c2': 1e-3,  # coefficient for L2 penalty
    'max_iterations': 50,  # stop earlier

    # include transitions that are possible, but not observed
    'feature.possible_transitions': True
})

saveto = args.sa_model
trainer.train(saveto)

tagger = pycrfsuite.Tagger()
tagger.open(saveto)

example_sent = test_sents[0]
print ' '.join(sent2tokens(example_sent))

print("Predicted:", ' '.join(tagger.tag(sent2features(example_sent))))
print("Correct:  ", ' '.join(sent2labels(example_sent)))

def bio_classification_report(y_true, y_pred):
    """
    Classification report for a list of BIO-encoded sequences.
    It computes token-level metrics and discards "O" labels.

    Note that it requires scikit-learn 0.15+ (or a version from github master)
    to calculate averages properly!
    """
    lb = LabelBinarizer()
    y_true_combined = lb.fit_transform(list(chain.from_iterable(y_true)))
    y_pred_combined = lb.transform(list(chain.from_iterable(y_pred)))

    tagset = set(lb.classes_) - {'O'}
    tagset = sorted(tagset, key=lambda tag: tag.split('-', 1)[::-1])
    class_indices = {cls: idx for idx, cls in enumerate(lb.classes_)}

    return classification_report(
        y_true_combined,
        y_pred_combined,
        labels = [class_indices[cls] for cls in tagset],
        target_names = tagset,
    )

y_pred = [tagger.tag(xseq) for xseq in X_test]

print(bio_classification_report(y_test, y_pred))

from collections import Counter
info = tagger.info()

def print_transitions(trans_features):
    for (label_from, label_to), weight in trans_features:
        print("%-6s -> %-7s %0.6f" % (label_from, label_to, weight))

print("Top likely transitions:")
print_transitions(Counter(info.transitions).most_common(15))

print("\nTop unlikely transitions:")
print_transitions(Counter(info.transitions).most_common()[-15:])


def print_state_features(state_features):
    for (attr, label), weight in state_features:
        print("%0.6f %-6s %s" % (weight, label, attr))

print("Top positive:")
print_state_features(Counter(info.state_features).most_common(20))

print("\nTop negative:")
print_state_features(Counter(info.state_features).most_common()[-20:])

#!/bin/sh

time python algos/crf.py \
  data/splits/train.sent \
  data/output/train.pos.brill \
  data/splits/train.tags \
  data/splits/valid.sent \
  data/output/valid.pos.brill \
  data/splits/valid.tags

time python algos/crf.py \
  data/splits/train.sent \
  data/output/train.pos.brill \
  data/splits/train.tags \
  data/splits/valid.sent \
  data/output/valid.pos.brill \
  data/splits/valid.tags \
  --skip_pos

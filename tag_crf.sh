#!/bin/sh

python algos/crf_tag.py \
--tr_sent data/splits/train.sent \
--tr_pos data/output/train.pos.brill \
--tr_tags data/splits/train.tags \
--va_sent data/splits/valid.sent \
--va_pos data/output/valid.pos.brill \
--va_tags data/splits/valid.tags \
--te_sent data/splits/test.sent \
--te_pos data/output/test.pos.brill \
--te_tags data/splits/test.tags \
--sa_model conll2002-esp.crfsuite \
--sa_train data/output/train.tags.crf \
--sa_valid data/output/valid.tags.crf \
--sa_test data/output/test.tags.crf

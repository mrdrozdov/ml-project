#!/bin/sh

git clone git@github.com:mesnilgr/is13.git
virtualenv .venv
. .venv/bin/activate
pip install -r requirements.txt

Example Usage (works for crf, maxent, elman, jordan)

virtualenv .venv
source .venv/bin/activate
sh run_crf.sh
sh tag_crf.sh
sh eval_crf.sh

---

Attribution

The is13 directory (jordan, elman) is from https://github.com/mesnilgr/is13

The crf is from https://github.com/tpeng/python-crfsuite

The maxent is from https://github.com/arne-cl/nltk-maxent-pos-tagger

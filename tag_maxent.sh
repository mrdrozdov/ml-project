#!/bin/sh

python algos/maxent_tag.py \
--it 50 \
--tr_sent data/splits/train.sent \
--tr_tags data/splits/train.tags \
--va_sent data/splits/valid.sent \
--va_tags data/splits/valid.tags \
--te_sent data/splits/test.sent \
--te_tags data/splits/test.tags \
--sa_model data/models/maxent.pkl \
--sa_train data/output/train.tags.maxent \
--sa_valid data/output/valid.tags.maxent \
--sa_test data/output/test.tags.maxent

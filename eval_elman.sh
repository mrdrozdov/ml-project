#!/bin/sh

python conll.py \
  data/splits/train.sent \
  data/output/train.pos.brill \
  data/splits/train.tags \
  data/output/train.tags.elman > data/output/train.conll.elman

python conll.py \
  data/splits/valid.sent \
  data/output/valid.pos.brill \
  data/splits/valid.tags \
  data/output/valid.tags.elman > data/output/valid.conll.elman

python conll.py \
  data/splits/test.sent \
  data/output/test.pos.brill \
  data/splits/test.tags \
  data/output/test.tags.elman > data/output/test.conll.elman

perl conlleval.pl < data/output/train.conll.elman
perl conlleval.pl < data/output/valid.conll.elman
perl conlleval.pl < data/output/test.conll.elman

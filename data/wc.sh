#!/bin/sh
ls output | xargs -I {} wc output/{}
ls splits | xargs -I {} wc splits/{}

# wc output/train.pos.brill
# wc output/valid.pos.brill
# wc output/test.pos.brill
# wc splits/train.sent
# wc splits/valid.sent
# wc splits/test.sent

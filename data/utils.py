import os

def absjoin(*paths):
   return os.path.abspath(os.path.join(*paths))

thisdir = os.path.split(__file__)[0]

def data_file(name):
   return absjoin(thisdir, 'data', name)

from itertools import chain
import nltk
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.preprocessing import LabelBinarizer
import sklearn
import pycrfsuite

import numpy
import time
import sys
import subprocess
import os
import random

from is13.data import load
from is13.rnn.elman import model
from is13.metrics.accuracy import conlleval
from is13.utils.tools import shuffle, minibatch, contextwin

print(sklearn.__version__)

# File Utils

def absjoin(*paths):
   return os.path.abspath(os.path.join(*paths))

thisdir = os.path.split(__file__)[0]

def data_file(name):
   return absjoin(thisdir, 'splits', name)


# load the dataset
fold = 3
train_set, valid_set, test_set, dic = load.atisfold(fold)
idx2label = dict((k,v) for v,k in dic['labels2idx'].iteritems())
idx2word  = dict((k,v) for v,k in dic['words2idx'].iteritems())

train_lex, train_ne, train_y = train_set
valid_lex, valid_ne, valid_y = valid_set
test_lex,  test_ne,  test_y  = test_set

train_sents = [[(idx2word[w], idx2label[y]) for w, y in zip(words, labels)] for words, labels in zip(train_lex, train_y)]
valid_sents = [[(idx2word[w], idx2label[y]) for w, y in zip(words, labels)] for words, labels in zip(valid_lex, valid_y)]
test_sents = [[(idx2word[w], idx2label[y]) for w, y in zip(words, labels)] for words, labels in zip(test_lex, test_y)]

with open(data_file("train.sent"), "wb") as f_sent, open(data_file("train.tags"), "wb") as f_tags:
  for s in train_sents:
    words, tags = zip(*s)
    f_sent.write(" ".join(words) + "\n")
    f_tags.write(" ".join(tags) + "\n")

with open(data_file("valid.sent"), "wb") as f_sent, open(data_file("valid.tags"), "wb") as f_tags:
  for s in valid_sents:
    words, tags = zip(*s)
    f_sent.write(" ".join(words) + "\n")
    f_tags.write(" ".join(tags) + "\n")

with open(data_file("test.sent"), "wb") as f_sent, open(data_file("test.tags"), "wb") as f_tags:
  for s in test_sents:
    words, tags = zip(*s)
    f_sent.write(" ".join(words) + "\n")
    f_tags.write(" ".join(tags) + "\n")
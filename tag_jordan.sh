#!/bin/sh
THEANO_FLAGS="device=cpu" python is13/examples/jordan-classify.py data/splits/train.sent > data/output/train.tags.jordan
THEANO_FLAGS="device=cpu" python is13/examples/jordan-classify.py data/splits/valid.sent > data/output/valid.tags.jordan
THEANO_FLAGS="device=cpu" python is13/examples/jordan-classify.py data/splits/test.sent > data/output/test.tags.jordan
